class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password
    def display1(self):
        print("(parent class/User) Username:", self.username, "password:", self.password)

class Admin(User):
    def display2(self):
        print("(subclass class/Admin) Username:", self.username, "password:", self.password)

a_1 = Admin("sherlockhomie", "watsonsucks123")
u_45 = User("prettyme", "condor88")

u_45.display1()
a_1.display2()
