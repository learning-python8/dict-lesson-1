class ClubMembers:
    def __init__(self, name, birthday, age, favoriteFood, goal):
        self.name = name
        self.birthday = birthday
        self.age = age
        self.favoriteFood = favoriteFood
        self.goal = goal
    def displaymem(self):
        print("name: ", self.name)
        print("birthday: ", self.birthday)
        print("age: ", self.age)
        print("favorite food: ", self.favoriteFood)
        print("goal: ", self.goal)
class ClubOfficers(ClubMembers):
    def __init__(self, name, birthday, age, favoriteFood, goal, position):
        self.name = name
        self.birthday = birthday
        self.age = age
        self.favoriteFood = favoriteFood
        self.goal = goal
        self.position = position
    def displaymem2(self):
        print("name: ", self.name)
        print("birthday: ", self.birthday)
        print("age: ", self.age)
        print("favorite food: ", self.favoriteFood)
        print("goal: ", self.goal)
        print("position: ", self.position)

m_1 = ClubMembers("Tom", "January 16", 14, "Ice cream", "to be happy")

m_1.displaymem()

o_4 = ClubOfficers("Vera", "June 22", 16, "Beef stroganoff", "to be the world's greatest chef", "Treasurer")

o_4.displaymem2()